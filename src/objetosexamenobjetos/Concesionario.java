/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosexamenobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Concesionario {
    private Coche[] espacioGaraje;
    private int cantidadCochesEnStock;
    
    public Concesionario (int espacio){
        this.espacioGaraje = new Coche[espacio];
        cantidadCochesEnStock = 0;
    }
    
    public void aniadirCoche (Coche coche) throws GarajeLlenoException{
        try{
            this.espacioGaraje[this.cantidadCochesEnStock] = coche;
            this.cantidadCochesEnStock++;
        }catch(java.lang.ArrayIndexOutOfBoundsException ex){
            throw new GarajeLlenoException(this.espacioGaraje.length);
        }
    }
    
    public void quitarCoche (int posicionCoche){
        this.espacioGaraje[posicionCoche] = null;
        this.cantidadCochesEnStock--;
        for(int i = posicionCoche;i<this.cantidadCochesEnStock-posicionCoche;i++){
            this.espacioGaraje[i] = this.espacioGaraje[i+1];
        }
    }
    
    public int getCapacidad(){
        return this.espacioGaraje.length;
    }
    
    public Coche getCoche(int index){
        return this.espacioGaraje[index];
    }
    
    public int getCochesEnStock(){
        return this.cantidadCochesEnStock;
    }

}
