/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosexamenobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class OpelCorsa extends Coche{
    public OpelCorsa (String color){        
        this.color = color;
        this.combustible = "diesel";
        this.puertas = 3;
        this.modelo = this.getClass().getSimpleName();
    }
}
