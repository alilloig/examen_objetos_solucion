/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosexamenobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public abstract class Coche {
    protected String color;
    protected int puertas;
    protected String combustible;
    protected String modelo;
  
    public String getColor(){
       return this.color;
    }
    
    public int getPuertas(){
        return this.puertas;
    }
    
    public String getCombustible(){
        return this.combustible;
    }

    public String getModelo() {
        return this.modelo;
    }
}
