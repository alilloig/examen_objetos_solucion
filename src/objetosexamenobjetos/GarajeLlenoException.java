/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosexamenobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class GarajeLlenoException extends Exception {
    private int tamanioGaraje;
    
    public GarajeLlenoException(int length) {
        this.tamanioGaraje = length;
    }
    @Override
    public String toString(){
        return ("Garaje lleno, las "+this.tamanioGaraje+" plazas se encuentran ocupadas");
    }
}
