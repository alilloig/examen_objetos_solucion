/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosexamenobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class ToyotaAuris extends Coche {
    public ToyotaAuris(String color){
        this.color = color;
        this.combustible = "hibrido gasolina";
        this.puertas = 5;
        this.modelo = this.getClass().getSimpleName();
    }
}
